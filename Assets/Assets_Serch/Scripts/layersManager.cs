﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class layersManager : MonoBehaviour
{
    [Header("Floor Layer")]
    [SerializeField] GameObject[] _primitives; //0 -> Circle y 1 -> Rectangle
    [Header("Wall Layer")]
    [SerializeField] Behaviour _pointsScript;
    
    public bool _setPrimitive{get; set;} //Se activa cuando selecciona el floor layer

    int index;
    PrimitiveActions _primitiveEnabled;
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && this.gameObject.activeSelf)
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                if(hitInfo.collider.tag == "primitive"){
                    _primitiveEnabled =  hitInfo.collider.gameObject.GetComponent<PrimitiveActions>();
                    _primitiveEnabled.components();
                } else if(_setPrimitive){
                    Instantiate(_primitives[index],new Vector3(hitInfo.point.x,0.0f, hitInfo.point.z),Quaternion.identity);
                }
            }
        }
    }

    public void enableFloorActions(int value){
        _setPrimitive = true; //Para colocar primitivas
        if(_primitiveEnabled != null && _primitiveEnabled._canvas.activeSelf){ //Para desactivar el canvas de la primitiva, si es el caso
            _primitiveEnabled.components();
        }
        index = value; ////0 -> Circle y 1 -> Rectangle (Para instanciar)
    }

   /* public void floorActions(string value){
        switch (value)
        {
            case "Points":
            _pointsScript.enabled = true;
            //desactivar script para instanciar prefabs de primitivas
            break;

            case "Circle":
            //Script para instanciar el prefab de la primitiva
            _pointsScript.enabled = false;
            break;

            case "Rectangle":
            //Script para instanciar el prefab de la primitiva
            _pointsScript.enabled = false;
            break;
        }
    }*/
}
