﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace UIM
{
    public class UIManager : MonoBehaviour
    {
        [Header("SideMenu animators")]
        [SerializeField] Animator _animText;
        [SerializeField] Animator _animMenu;
        [Header("SideMenu buttons")]
        [SerializeField] GameObject[] _buttons;
        [Header("SideMenu images")]
        [SerializeField] Sprite[] _mainImages;
        [SerializeField] Sprite[] _images;
        [Header("SideMenu texts")]
        [SerializeField] TextMeshProUGUI[] _texts;
        [Header("Background image")]
        [SerializeField] Image _backImage;
        [Header("Designa image")]
        [SerializeField] Image _designaLogo;
        [Header("Highlight image")]
        [SerializeField] GameObject _highlights;
        [SerializeField] Transform _highlightSelected;
        [SerializeField] List<GameObject> _highlightPositions;
        [Header("Areas buttons")]
        [SerializeField] GameObject[] _areaButton;
        [Header("Help button")]
        [SerializeField] Image _helpButton;
        [SerializeField] Sprite _mainImage;
        [SerializeField] Sprite _selectedImage;
        [Header("Help Messages")]
        [SerializeField] GameObject[] _message;

        ShowPopUp _showPU;
        bool isShowing = false;//is showing sideMenu?
        bool isMoving = false;
        bool isShowingHelp = false;
        int _currentItem;

        bool _errorAnim= true;

        private void Start(){
            _showPU=GetComponent<ShowPopUp>();
        }

        private void Update(){
            
            if(isMoving && _highlightSelected.transform.localPosition.y !=_highlightPositions[_currentItem].transform.localPosition.y){
                _highlightSelected.transform.localPosition = 
                Vector2.Lerp(_highlightSelected.transform.localPosition,
                _highlightPositions[_currentItem].transform.localPosition,0.4f);
            }

            if(isShowingHelp){
                if(Input.GetMouseButtonDown(0)){
                    showAndHideHelpWindow(false);
                }
            }

            //Este es un condicional de plan de contingencia XD (Para la animación del sidemenu)
            if(!isShowing && _errorAnim){
                scaleMenu();
                _errorAnim = false;
            }
        }

        public void itemSelected(int item){ //Para WEB
            _currentItem = item;
            if(isShowing && item != 4){

                if(!_highlights.activeSelf){
                    StartCoroutine(delayAndShow(0.4f,_highlights));
                }

                isMoving = true;

            }else if(!isShowing){

                _animText.SetBool("Showtext",true); 
                _animMenu.SetBool("Scalemenu",true);

                StartCoroutine(delayAndShowOrHide(0.4f,_designaLogo));//Show logo 
                isShowing=true;
                _errorAnim = true; //Para gestionar un posible error con la animación del sideMenu  
                if(item != 4){
                    
                    StartCoroutine(delayAndShow(0.4f,_highlights));
                    isMoving = true;
                
                }else{isMoving = false;}

            }else if(item == 4){isMoving=false;}
        }

        public void scaleMenu(){ //Para WEB
                
                if(_highlights.activeSelf){
                    _highlights.SetActive(false);
                }

                isMoving=false;

                _animText.SetBool("Showtext",false);
                _animMenu.SetBool("Scalemenu",false);

                _designaLogo.enabled=false;
                isShowing=false;
        }

        /*public void scaleMenu(){ //Para MOVIL
            if(!isShowing){
                _animText.SetBool("Showtext",true); 
                _animMenu.SetBool("Scalemenu",true);
                StartCoroutine(delayAndShowOrHide(0.4f,_designaLogo));//Show logo 
                isShowing=true;
            }else{
                _animText.SetBool("Showtext",false);
                _animMenu.SetBool("Scalemenu",false);
                _designaLogo.enabled=false;
                isShowing=false;}
        }*/

        public void changeItemColor(int value){
            
            if(!PlayerPrefs.HasKey("Onboarding"+(value).ToString())){
                _showPU.onBoardingManager(value); 
            }

            if(_texts[4].gameObject.activeSelf){ //hide intro text
                _texts[4].gameObject.SetActive(false);
                showVenue();
            }

            for(int i = 0;i < _buttons.Length;i++){
                if(i == value){ //show current selection
                    _buttons[i].GetComponent<Image>().sprite = _images[i];
                    _texts[i].color = new Color32(243,156,18,255);
                }else{ 
                    _buttons[i].GetComponent<Image>().sprite = _mainImages[i];
                    _texts[i].color = new Color32(179,179,179,255);
                }
             
                if(i == value && i < 3){ //Show and hide area buttons and help button
                    _areaButton[i].SetActive(true);
                    //_helpButton.enabled = true;
                    StartCoroutine(delayAndShowOrHide(0.2f,_helpButton)); 
                }else if(i != value && i < 3){
                    _areaButton[i].SetActive(false);
                    _helpButton.enabled = false;
                }
            }
        }

        public void showAndHideHelpWindow(bool value){
            if(!value){
                _helpButton.sprite = _mainImage;
                isShowingHelp = value;
            }else{
                _helpButton.sprite = _selectedImage;
                isShowingHelp = value;
                }
            
            _message[_currentItem].SetActive(value);
        }

        void showVenue(){
            _backImage.raycastTarget = false;
            StartCoroutine(fadeOut(1));
        }
	    IEnumerator fadeOut(float value){
     	    while (value > 0)
     	    {
                _backImage.color = new Color(255,255,255,value);
                yield return new WaitForSeconds(0.025f);
                value = value - 0.01f;
     	    }
	    }
        IEnumerator delayAndShowOrHide(float time, Behaviour item){
            yield return new WaitForSeconds(time);
            if(!item.enabled)
            {item.enabled = true;}
            else{item.enabled = false;}
        }
        IEnumerator delayAndShow(float time, GameObject item){
            yield return new WaitForSeconds(time);
            if(!item.activeSelf)
            {
                item.SetActive(true);
            }else{item.SetActive(false);}
        }
    }
}

