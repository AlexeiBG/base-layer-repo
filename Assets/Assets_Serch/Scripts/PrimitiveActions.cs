﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrimitiveActions : MonoBehaviour
{

    [SerializeField] Transform _primitive;
    [Header("Private UI")]
    public GameObject _canvas;
    [SerializeField] Image _moveButton;
    [SerializeField] Image _moveImage;
    [SerializeField] Image _scaleButton;

    RectTransform _canvasRect;
    Vector3 mousePos;
    Collider _collider;
    GameObject target;
    bool isMoving = false;
    bool isActive = false;
    float value = 0f;
    
    layersManager layerM;

    private void Start(){
        target = this.gameObject;
        _canvasRect = _canvas.GetComponent<RectTransform>();
        layerM = GameObject.Find("Layers_Actions").GetComponent<layersManager>();
    }

    private void Update(){
        if(isMoving){
            if(Input.GetMouseButtonUp(0)){
                _scaleButton.enabled = true; //Enable UI
                _moveButton.enabled = true;
                _moveImage.enabled = false;
                isMoving = false;
            }
        }
    }

    public void scale(){
        //Disable button
        if(_moveButton.enabled){_moveButton.enabled = false;}
        //Resize
        if(Input.GetAxis("Mouse X") > 0){ //Si el mouse se mueve a la derecha
                target.transform.localScale += new Vector3(0.1f,0,0.1f);
                _canvasRect.sizeDelta = new Vector2(_canvasRect.rect.width+3.6f,_canvasRect.rect.height+3.6f);
        }else{
            if(target.transform.localScale.x > 1){
                target.transform.localScale -= new Vector3(0.1f,0,0.1f);
                _canvasRect.sizeDelta = new Vector2(_canvasRect.rect.width-3.6f,_canvasRect.rect.height-3.6f);
            }
        }
    }

    public void move(){
        //Disable buttons
        if(_moveButton.enabled){
            _moveButton.enabled = false;
            _scaleButton.enabled = false;
            _moveImage.enabled = true;
        }
        isMoving = true; //flag
        //Move primitive
        mousePos=Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _primitive.position = new Vector3(mousePos.x,0,mousePos.z);
    }

    public void enableUI(){
        if(!_moveButton.enabled){
            _moveButton.enabled = true;
        }
    }
    
    public void components(){

        if(!_canvas.activeSelf){
            _canvas.SetActive(true);
            others();
                            //Desactivar opciones de la UI general
        }else{
            _canvas.SetActive(false);
            isActive = false;
            layerM._setPrimitive = true;
                            //Activar opciones de la UI general
        }
    }

    public bool isActiveThis(){
        return isActive;
    }

    private void others(){
        GameObject[] array = GameObject.FindGameObjectsWithTag("primitive");
        foreach (var item in array)
        {
            if(item.GetComponent<PrimitiveActions>().isActiveThis()){
                item.GetComponent<PrimitiveActions>().components(); //Desactiva canvas y trigger de los otros
            }
        }
        isActive = true;
        layerM._setPrimitive = false;
    }
}
