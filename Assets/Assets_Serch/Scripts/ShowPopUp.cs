﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShowPopUp : MonoBehaviour
{
    [SerializeField] List<GameObject> _popUps;
    [SerializeField] Animator background;

    bool _popActive;
    bool _isChecked=false;
    
    void Awake(){
        _popActive=false;
        popUpManager(4); //Welcome popup
    }

    public void popUpManager(int index){
        if(!_popActive){
            _popUps[index].SetActive(true);
            _popActive=true;
            background.SetBool("Showpanel",true);
        }else{
            StartCoroutine(disable(index));
            _popActive=false;
            background.SetBool("Showpanel",false);
        }
    }

    public void onBoardingManager(int index){
        if(!_popActive){
            _popUps[index].SetActive(true);
            _popActive=true;
            background.SetBool("Showpanel",true);
        }else{
            StartCoroutine(disable(index));
            _popActive=false;
            background.SetBool("Showpanel",false);
            if(_isChecked){
                PlayerPrefs.SetString("Onboarding"+index.ToString(),"showed");
                PlayerPrefs.Save();
                _isChecked=false;
            }

        }
    }

    public void checkOnboarding(Image _checkImage){
        if(!_checkImage.enabled){
            _checkImage.enabled=true;
            _isChecked=true;
        }else{
            _checkImage.enabled=false;
            _isChecked=false;
        }
    }

    IEnumerator disable(int value){
         yield return new WaitForSeconds(0.8f);
         _popUps[value].SetActive (false);
    }
}
