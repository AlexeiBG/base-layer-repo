﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class PointScriptOriginal : MonoBehaviour
{

    [Header("new ones")]
    CustomGrid grid;
    public bool ZoneCompleted = false; 
    public List<Vector3> verticesList;  
    public int[] triangleArray = new int[6];  
    private Mesh newMesh; 
    public int[] newtriangleArray;

    public int testy;

    public int NumZones=2;
    [SerializeField]
    public NodeZone2 ForbiddenZones; 
    public NodeZone2 WallZones; 
    //STUFF FROM THE old script
    public GameObject prefabQuadPivot; 

    [Header("Settings")]
    //public RectTransform crosshair;
    public int counter = 0;
    public float SmallesDistanceAccepted = .5f;
    //public bool RoomFinished = false;
    //public bool ScalingWallsFinished = false;
    public List<Transform> points;
    public GameObject ZonesGrandDaddy;
    public List<GameObject> ZonesParent;
    public int zonecounter = 0;
    public GameObject nodeToFind = null;
    public float NodeColliderRadius;
    private float duration = 10f;
    List<LinePivotScript> lps;
    public Material myMaterial;

    public void Awake()
    {
        grid = FindObjectOfType<CustomGrid>();
    }


    // Start is called before the first frame update
    void Start()
    {
        points = new List<Transform>();
        lps = new List<LinePivotScript>();
        //Create parent of nodes 
        MoveToNextZone();
         


    }


    // Update is called once per frame
    void Update()
    {

        //check what class of zone should be made
 


        //Clicks and zonedone
        if (Input.GetMouseButtonDown(0) && !ZoneCompleted)
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hitInfo))
            {
                Debug.Log("collision MOUSE 0 " + hitInfo.collider.tag);
                if (hitInfo.collider.tag!="zone")
                {
                    PlaceNde(hitInfo.point);
                }
 
            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                Debug.Log("hitinfo " + hitInfo.collider.name );
            }
        }


        if (Input.GetKeyDown(KeyCode.Q))
        {
            // MoveToNextZone();

        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            CalculatingTheTriangles(testy);
        }

    }

    /* This should know how many zones do we have, and move freely between them
     * use an int to change 
     */
    public void MoveBetweenZones(int num)
    {
        NumZones = num;

    }


    public void MoveToNextZone()
    {
        GameObject provisional = CreatingNewZone();
        ZonesParent.Add(provisional); 

    }


    public GameObject CreatingNewZone()
    {
        //new game object of all zones
        GameObject newzone = new GameObject();
        newzone.transform.position = new Vector3(0f, .2f, 0f);
        newzone.tag = "zone";
        newzone.AddComponent<ZONESCRIPT>();

        MeshFilter ZoneMesh = newzone.AddComponent<MeshFilter>();
        ZoneMesh.name = "ZoneMesh" + counter;

        MeshRenderer ZoneMRend = newzone.AddComponent<MeshRenderer>();
        newzone.name = "Zone " + zonecounter;

        MeshCollider zoneCOllider = newzone.AddComponent<MeshCollider>();

        //parent to zoneparent physically in hierarchy
        newzone.transform.SetParent(ZonesGrandDaddy.transform);
        //logically inside List of the ZoneClass
        //iterate in zones we have created. TODO
        if (NumZones == 1)
        {
            Debug.Log("NEW FORIBDDEN");
            ForbiddenZones.Zones.Add(newzone);
        }
        else if(NumZones == 2)
        {
            Debug.Log("NEW WALL");
            WallZones.Zones.Add(newzone);
        }
        else
        {
            Debug.Log("ERROR NO ZONE SELECTED");

        }



        //copy list to new go  

        return newzone;
    }


    private void PlaceNde(Vector3 clickPoint)
    {
        var finalPosition = grid.GetNearestPointOnGrid(clickPoint); //position in grid
       //z Debug.Log("FInal pos " + finalPosition);
        GameObject Point = (GameObject)Instantiate(prefabQuadPivot, finalPosition+new Vector3(0f,0.3f,0f), Quaternion.identity);

        Point.transform.SetParent(ZonesParent[zonecounter].transform);

        Point.name = "node" + counter;
        //Point.transform.LookAt(new Vector3(camera.transform.position.x, 0f, camera.transform.position.z));

        points.Add(Point.transform);

        verticesList.Add(finalPosition);
        //uvList.Add(clickPoint);

        counter += 1;

        if (counter > 1)
        {
             
            points[counter - 2].LookAt(points[counter - 1].position);

            float distance = Vector3.Distance(points[counter - 2].position, points[counter - 1].position);
//            Debug.Log("POINTSCRIPT _ node " + (counter - 1) + "to node " + (counter) + " distance " + distance);


            if (distance > SmallesDistanceAccepted)
            {
     
                LinePivotScript LinePivot = points[counter - 2].GetComponentInChildren<LinePivotScript>();
                LinePivot.gameObject.transform.localScale = new Vector3(.1f, .1f, distance);
                lps.Add(LinePivot);
            }



            //Check for distance between point 0 and last point added
            float minDistance = Vector3.Distance(points[0].position, points[counter - 1].position);
//            Debug.Log("Distance from first point to last point is " + minDistance);
            if (minDistance <= SmallesDistanceAccepted)
            {
               
                //Debug.Log("POINTSCRIPT _ node " + points[counter - 1].name + " should join node " + points[0].name);
                ZoneCompleted = true;
                //draw line

                float NewDistance = Vector3.Distance(points[counter - 2].position, points[0].position);
                points[counter - 2].name = "JOin this";
                points[0].name = "to this";

                points[counter - 2].transform.LookAt(points[0].position);
                //Debug.Log("POINTSCRIPT _ distance from  " + points[counter - 1].position + " to " + points[0].position);


                LinePivotScript LinePivot = points[counter - 2].GetComponentInChildren<LinePivotScript>();
                LinePivot.gameObject.transform.localScale = new Vector3(.1f, .1f, NewDistance);

                Destroy(points[counter-1].gameObject);
               // points.RemoveAt(counter - 1);
                lps.Add(LinePivot);

                //THIS is the end of the zone, proceed with next one
                createAMesh(counter-1, ZonesParent[zonecounter].gameObject);

                zonecounter++;
                counter = 0;

                 lps.Clear();
                 points.Clear();
                 ZoneCompleted = false;
                 verticesList.Clear();
               // uvList.Clear(); 
                 MoveToNextZone();
            }

        }



    }
 
    public void createAMesh(int counter, GameObject ZonePater)

    {
  
        Mesh mesh = ZonePater.GetComponent<MeshFilter>().mesh;

        //this one should erase the very very last
        verticesList.RemoveAt(counter);
 
        mesh.vertices = verticesList.ToArray();
          
        Vector2[] uvs = new Vector2[verticesList.Count];

        //Calculate the triangles according to the num of vertices
 
        CalculatingTheTriangles(verticesList.Count);
        //assign it
        mesh.triangles = triangleArray;

        mesh.RecalculateNormals();
        //Depending on zone number, set the material
        if (NumZones == 1)
        {
            ZonePater.GetComponent<MeshRenderer>().material = ForbiddenZones.zoneMaterial;
        }
        else
        {
            ZonePater.GetComponent<MeshRenderer>().material = WallZones.zoneMaterial;
        }
        ZonePater.GetComponent<MeshCollider>().sharedMesh = mesh;


        //NOW MAKE ANOTHEER VERTICES LIST; BUT IN DIFFEREMT ORDER
        GameObject mgo = new GameObject();
        mgo.tag = "zone";
        mgo.transform.SetParent(ZonePater.transform);
       
         
    }

    public void CalculatingTheTriangles(int vertixSize)
    {

        //Debug.Log("Calculating with size " + vertixSize);

        if (vertixSize % 2 == 0)
        {
           //Debug.Log("Pair");
            int trianglesize = vertixSize - 2;
            int triangeSizeArray = trianglesize * 3;

          //  Debug.Log("how many triangle " + trianglesize);
           // Debug.Log("triangle size this big " + triangeSizeArray);

            triangleArray = new int[triangeSizeArray];

            int first = 0;
            int second = 1;

            for (int i = 0; i < triangleArray.Length; i+=3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = first;
                    first++;
                }
               
            }

            for (int i = 1; i < triangleArray.Length; i += 3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = second;
                    second++;
                }
            }

            for (int i = 2; i < triangleArray.Length; i += 3)
            {
                triangleArray[i] = vertixSize-1;
            }
 
        }
        else
        {
          //  Debug.Log("Unpair");
            int trianglesize = vertixSize - 1;
            int triangeSizeArray = trianglesize * 3;

            triangleArray = new int[triangeSizeArray];

          //  Debug.Log("how many triangle " + trianglesize);
          //  Debug.Log("triangle size this big " + triangeSizeArray);

            int first = 0;
            int second = 1;

            for (int i = 0; i < triangleArray.Length; i += 3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = first;
                    first++;
                }

            }

            for (int i = 1; i < triangleArray.Length; i += 3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = second;
                    second++;
                }
            }

            for (int i = 2; i < triangleArray.Length; i += 3)
            {
                triangleArray[i] = vertixSize - 1;
            }


        }
 

        triangleArray[0] = 0;
        /*
        for (int i = 0; i < triangleArray.Length; i ++)
        {
            Debug.Log("trinagle array index  [" + i + "]  VAlue:" + triangleArray[i]);
        }
        */
    }
 
    public void ForceTrianglesToBeMultipleOf3(int cunt, int vertexcunt)
    {
        if (cunt % 3 == 0)
        {
            triangleArray = new int[cunt];
            int j = 0;
            bool once =false;
            for (int i = 0; i < cunt; i++)
            {
                if (j < vertexcunt && !once  )
                {
                  //  Debug.Log("TriArr[" + i + "]" + "(" + j + ")");
                    triangleArray[i] = j;
                    j++;

                }
                else
                {
                    once = true;
                    j--;
                    triangleArray[i] = j;
                 //   Debug.Log("TriArr[" + i + "]" + "(" + j + ")");
                }


            }



        }
        else
        {
           //Debug.Log("this Cunt is recursive :"+cunt);
            cunt++;
            ForceTrianglesToBeMultipleOf3(cunt, vertexcunt);
        }


    }


    //THIS will be a UI button on top of the made zone
    //
    public void ButtonDelete()
    {
        //Find the zone inside the Class and delete the index (logically)
        //find the nodes in parent and delete them gameobjects (physcally)


    }
 

    /*
    public void CtrlZ()
    {
        if (counter <= 0)
        {

            Debug.Log("do nothing");
        }
        else
        {

            Debug.Log("counter " + counter + " lps counter " + lps.Count);
            //Points list, remove last one
            nodeToFind = parentOFNodes.transform.Find("node" + (counter - 1)).gameObject;
            Destroy(nodeToFind);
            points.RemoveAt(counter - 1);
            lps.RemoveAt(lps.Count - 1);
            counter--;

            Debug.Log("trying to re scale line of node " + (counter));
            //reset pivotline before the last one
            LinePivotScript LastLine = points[counter - 1].GetComponentInChildren<LinePivotScript>();
            LastLine.gameObject.transform.localScale = new Vector3(.1f, .1f, .1f);
        }
    }

    public void EraseAllNodes()
    {
        if (counter <= 0)
        {

            Debug.Log("do nothing");
        }
        else
        {

            points.Clear();
            foreach (Transform t in parentOFNodes.transform)
            {
                GameObject.Destroy(t.gameObject);
            }
            counter = 0;
        }
    }


    */




}

[Serializable]
public class NodeZone2{

    public int id;
    public string type;
    public List<GameObject> Zones = new List<GameObject>();
    public Material zoneMaterial;

}
