﻿using UnityEngine.UI;
using UnityEngine;

public class NodoDistance : MonoBehaviour
{
    public Text nodoDistance;
    Camera main;
    public void Start()
    {
        main = FindObjectOfType<Camera>();
        //nodoDistance.GetComponent<Text>();
        nodoDistance.text = "";
    }

    public void setDistance(float distance) 
    { 
        nodoDistance.text = distance + "m";
        //nodoDistance.transform.LookAt(main.transform.position);
    }


}
