﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticleOnFloor : MonoBehaviour
{
    public GameObject Particles;
    public GameObject reticle;
    public GameObject crosshairFloor;


    public void FixedUpdate()
    {
        raycastTheFloor();
    }




    public void raycastTheFloor()
    {

        Ray ray = Camera.main.ScreenPointToRay(crosshairFloor.transform.position);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo))
        {
            if (hitInfo.collider.tag == "DebugPlane")
            {

                reticle.SetActive(true);


                Vector3 vec3 = hitInfo.point + (Vector3.up * 0.1f);



                reticle.transform.position = vec3;
                Debug.Log("ray info "+ray.ToString());

            
            }

        }
        else
        {
            Debug.Log("not hitting the plane!!");
            reticle.SetActive(false);
        }


    }


}
