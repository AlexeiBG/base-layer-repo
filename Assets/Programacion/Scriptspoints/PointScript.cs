﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class PointScript : MonoBehaviour
{

    CustomGrid grid;

    [Header("Generalities")]
    public bool ZoneCompleted = false; 
    public List<Vector3> verticesList;  
    public int[] triangleArray = new int[6];  
    private Mesh newMesh;
    public GameObject prefabQuadPivot;
    public GameObject PrefabButtonQuit;
    public GameObject quit;
    public int[] newtriangleArray;
    public int testy;
    [Tooltip("This refers to the index of the zones, 1-Forbidden, 2-Wall, 3-Ceiling")]
    public int NumZones=2;
   
    [Header("ForbiddenZoneSettings")]
    [SerializeField]
    public NodeZone ForbiddenZones;
    public GameObject ZonesFrobidden;
    public List<GameObject> ZonesParentForbidden;
    public int zonecounterF = 0;

    [Header("Wallzones settings")]
    public NodeZone WallZones; 
    //STUFF FROM THE old script∫
    public GameObject Zoneswallz;
    // public List<GameObject> ZonesParent;
    public List<GameObject> ZonesParentWalls;
    public int zonecounterW = 0;

    [Header("Ceiling")]
    public Vector3 height;
    public Material ceilingMaterial;
    public GameObject ParenCeiling;
    public List<GameObject> ceilingList;
    public Material myMaterial;


    [Header("Settings")]
    //public RectTransform crosshair;
    public int counter = 0;
    public bool secondnode = false;
    public float SmallesDistanceAccepted = .5f;
    //public bool RoomFinished = false;
    //public bool ScalingWallsFinished = false;
    public List<Transform> points;
    public GameObject nodeToFind = null;
    public float NodeColliderRadius;
    private float duration = 10f;
    List<LinePivotScript> lps;



    public void Awake()
    {
        grid = FindObjectOfType<CustomGrid>();
    }
 
    // Start is called before the first frame update
    void Start()
    {
        points = new List<Transform>();
        lps = new List<LinePivotScript>();
        //Create parent of nodes 
        //MoveToNextZone(NumZones);
        CreatingNewZone(NumZones);
    }
 
    // Update is called once per frame
    void Update()
    {

        //check what class of zone should be made
 
        //Clicks and zonedone
        if (Input.GetMouseButtonDown(0) && !ZoneCompleted)
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hitInfo))
            {
                //Debug.Log("collision MOUSE 0 " + hitInfo.collider.tag);
                if (hitInfo.collider.tag != "zone" && hitInfo.collider.tag != "zoneQuit" && NumZones != 3  )
                {
                    PlaceNde(hitInfo.point);
                }
                else if ( hitInfo.collider.tag != "zoneQuit" && NumZones == 3)
                {
                     PlaceCeilingNode(hitInfo.point);
                }
 
            }

        }
        else if ( Input.GetMouseButtonDown(0) && ZoneCompleted ) 
        {
            Debug.Log("zone has already been completed!");
           ///check if we are in the same zone
            //MoveToNextZone(NumZones);

        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                Debug.Log("hitinfo " + hitInfo.collider.name );
            }
        }


        if (Input.GetKeyDown(KeyCode.Q))
        {
            // MoveToNextZone();

        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            //CalculatingTheTriangles(testy);
        }

    }

    /* This should know how many zones do we have, and move freely between them
     * use an int to change 
     */
    public void MoveBetweenZones(int num)
    {
        NumZones = num;
        if (NumZones == 2)//selecting walls
        {

            CreatingNewZone(NumZones);
            
        }

        else if (NumZones == 1) //THis is forbidden
        {

            CreatingNewZone(NumZones);
        }
        else if (NumZones == 3)
        {
            Debug.Log("We want to create ceiling");


        }

    }
 /*
    public void MoveToNextZone(int classzone)
    {
        Debug.Log("move to next zone called");
        //GameObject provisionaldad = CreatingNewZone(classzone);
       
       
        if (provisionaldad != null)
        {
            Debug.Log("not a null dad");
            ZonesParent.Add(provisionaldad);
        }


    }
 */
    public void CreatingNewZone(int classzone)
    {

        //logically inside List of the ZoneClass
        //iterate in zones we have created. TODO
        if (classzone == 1)
        {
            Debug.Log("Creating a Forbiden zone");

            //new game object of all zones
            GameObject newzone = new GameObject();
            newzone.transform.position = new Vector3(0f, .2f, 0f);
            newzone.tag = "zone";
            newzone.AddComponent<ZONESCRIPT>();

            MeshFilter ZoneMesh = newzone.AddComponent<MeshFilter>();
            ZoneMesh.name = "ZoneMesh" + counter;

            MeshRenderer ZoneMRend = newzone.AddComponent<MeshRenderer>();
            newzone.name = "ZoneF" + zonecounterF;

            MeshCollider zoneCOllider = newzone.AddComponent<MeshCollider>();

            //parent to zoneparent physically in hierarchy
            newzone.transform.SetParent(ZonesFrobidden.transform);
            ZonesParentForbidden.Add(newzone);
            Debug.Log("NEW FORIBDDEN");
            ForbiddenZones.Zones.Add(newzone);
 
        }
        else if(classzone == 2)
        {
            //new game object of all zones
            Debug.Log("Class selected walls");

            GameObject newzone = new GameObject();
            newzone.transform.position = new Vector3(0f, .2f, 0f);
            newzone.tag = "zone";
            newzone.AddComponent<ZONESCRIPT>();

            MeshFilter ZoneMesh = newzone.AddComponent<MeshFilter>();
            ZoneMesh.name = "ZoneMeshW" + counter;

            MeshRenderer ZoneMRend = newzone.AddComponent<MeshRenderer>();
            newzone.name = "ZoneW" + zonecounterW;

            MeshCollider zoneCOllider = newzone.AddComponent<MeshCollider>();

            //parent to zoneparent physically in hierarchy
            newzone.transform.SetParent(Zoneswallz.transform);
            ZonesParentWalls.Add(newzone);
            Debug.Log("NEW WALL");
            WallZones.Zones.Add(newzone);


        }
        else
        {
            Debug.Log("ERROR NO ZONE SELECTED");


        }
 
    }
     
    private void PlaceNde(Vector3 clickPoint)
    {
        var finalPosition = grid.GetNearestPointOnGrid(clickPoint); //position in grid

        if (NumZones == 1) //FORBIDDEN
        {
            if (counter <= 0)
            {
                Debug.Log("This is the origin");
                PutANodeRetF(finalPosition);
               
            }
            else if (counter >= 1)
            {
                PutANode(finalPosition);

                Debug.Log("Counter bigger than 1");
                points[counter - 2].LookAt(points[counter - 1].position);

                float distance = Vector3.Distance(points[counter - 2].position, points[counter - 1].position);
               
                if (distance > SmallesDistanceAccepted)
                {
                    LinePivotScript LinePivot = points[counter - 2].GetComponentInChildren<LinePivotScript>();
                    LinePivot.gameObject.transform.localScale = new Vector3(.1f, .1f, distance);
                    lps.Add(LinePivot);
                }



                //Check for distance between point 0 and last point added
                float minDistance = Vector3.Distance(points[0].position, points[counter - 1].position);
               
                /*Only when the first triangle can be completed
                 */              
                if (minDistance <= SmallesDistanceAccepted && counter >= 3 )
                {
                    Debug.Log("Small distane and counter is bigger than 3");
                    //Debug.Log("POINTSCRIPT _ node " + points[counter - 1].name + " should join node " + points[0].name);
                    ZoneCompleted = true;
                    //draw line

                    float NewDistance = Vector3.Distance(points[counter - 2].position, points[0].position);
                    points[counter - 2].name = "JOin this";
                    points[0].name = "to this";

                    points[counter - 2].transform.LookAt(points[0].position);
                    //Debug.Log("POINTSCRIPT _ distance from  " + points[counter - 1].position + " to " + points[0].position);


                    LinePivotScript LinePivot = points[counter - 2].GetComponentInChildren<LinePivotScript>();
                    LinePivot.gameObject.transform.localScale = new Vector3(.1f, .1f, NewDistance);

                    Destroy(points[counter - 1].gameObject);
                    // points.RemoveAt(counter - 1);
                    lps.Add(LinePivot);

                    //THIS is the end of the zone, proceed with next one
                    createAMesh(counter - 1, ZonesParentForbidden[zonecounterF].gameObject);

                    zonecounterF++;
                    counter = 0;
                    quit.SetActive(true);
                    lps.Clear();
                    points.Clear();

                    verticesList.Clear();
                    //uvList.Clear(); 
                    ZoneCompleted = false;

                    CreatingNewZone(1); 
                }




            }
                    
        }
        else if(NumZones == 2)
        {
            if (counter <= 0)
            {
                Debug.Log("This is the origin of walls");
                PutANodeRetW(finalPosition);

            }
            else if (counter >= 1)
            {
                Debug.Log("counter bigger than 1");
               
                PutANode_in_wall(finalPosition);
                ZoneCompleted = true;
                points[counter - 2].LookAt(points[counter - 1].position);

                float distance = Vector3.Distance(points[counter - 2].position, points[counter - 1].position);

                if (distance > SmallesDistanceAccepted)
                {
                    LinePivotScript LinePivot = points[counter - 2].GetComponentInChildren<LinePivotScript>();
                    LinePivot.gameObject.transform.localScale = new Vector3(.1f, .1f, distance);
                    lps.Add(LinePivot);
                }

                zonecounterW++;
                counter = 0;
                quit.SetActive(true);
                lps.Clear();
                points.Clear();

                verticesList.Clear(); 
                ZoneCompleted = false;

                CreatingNewZone(2);
            }
            /*
            if (counter  == 2)
            {
                Debug.Log("Small distance and counter is equal 2");
                //Debug.Log("POINTSCRIPT _ node " + points[counter - 1].name + " should join node " + points[0].name);
                ZoneCompleted = true;
                //draw line

                float NewDistance = Vector3.Distance(points[counter - 2].position, points[0].position);
                points[counter - 2].name = "JOin this";
                points[0].name = "to this";

                points[counter - 2].transform.LookAt(points[0].position);
                //Debug.Log("POINTSCRIPT _ distance from  " + points[counter - 1].position + " to " + points[0].position);


                LinePivotScript LinePivot = points[counter - 2].GetComponentInChildren<LinePivotScript>();
                LinePivot.gameObject.transform.localScale = new Vector3(.1f, .1f, NewDistance);

                Destroy(points[counter - 1].gameObject);
                // points.RemoveAt(counter - 1);
                lps.Add(LinePivot);

                //THIS is the end of the zone, proceed with next one
               // createAMesh(counter - 1, ZonesParentWalls[zonecounterF].gameObject);

              //  zonecounterW++;

                counter = 0;
                quit.SetActive(true);
                lps.Clear();
                points.Clear();

                verticesList.Clear();
                //uvList.Clear(); 
                ZoneCompleted = false;

                CreatingNewZone(2);

            }
            */

        }

    }
 
    public void PutANode(Vector3 finalPosition)
    {

        GameObject Point = (GameObject)Instantiate(prefabQuadPivot, finalPosition + new Vector3(0f, 0.3f, 0f), Quaternion.identity);

        Point.transform.SetParent(ZonesParentForbidden[zonecounterF].transform);

        Point.name = "node" + counter;
        //Point.transform.LookAt(new Vector3(camera.transform.position.x, 0f, camera.transform.position.z));

        points.Add(Point.transform);

        verticesList.Add(finalPosition);
        //Instantiate the UI button to kill this thing

        counter++;
    }

    public void PutANode_in_wall(Vector3 finalPosition)
    {

        GameObject Point = (GameObject)Instantiate(prefabQuadPivot, finalPosition + new Vector3(0f, 0.3f, 0f), Quaternion.identity);

        Point.transform.SetParent(ZonesParentWalls[zonecounterW].transform);

        Point.name = "node" + counter;
        //Point.transform.LookAt(new Vector3(camera.transform.position.x, 0f, camera.transform.position.z));

        points.Add(Point.transform);

        verticesList.Add(finalPosition);
        //Instantiate the UI button to kill this thing

        counter++;
    }



    public void PutANodeRetF(Vector3 finalPosition)
    {

        GameObject Point = (GameObject)Instantiate(prefabQuadPivot, finalPosition + new Vector3(0f, 0.3f, 0f), Quaternion.identity);

        quit = Instantiate(PrefabButtonQuit, finalPosition + new Vector3(1f, 0.5f, 1f), Quaternion.identity, ZonesFrobidden.transform);
        quit.transform.SetParent(Point.transform);
        quit.GetComponent<ButtonQuitScript>().PassInfo(zonecounterF, "ZoneF"+zonecounterF, 1); //1 forbidden
 
        Point.transform.SetParent( ZonesParentForbidden[zonecounterF].transform );

        Point.name = "node" + counter;
        //Point.transform.LookAt(new Vector3(camera.transform.position.x, 0f, camera.transform.position.z));

        points.Add(Point.transform);

        verticesList.Add(finalPosition);
        //Instantiate the UI button to kill this thing
        quit.SetActive(false);

        counter++; 
    }

    public void PutANodeRetW(Vector3 finalPosition)
    {

        GameObject Point = (GameObject)Instantiate(prefabQuadPivot, finalPosition + new Vector3(0f, 0.3f, 0f), Quaternion.identity);

        quit = Instantiate(PrefabButtonQuit, finalPosition + new Vector3(1f, 0.5f, 1f), Quaternion.identity, Zoneswallz.transform);
        quit.transform.SetParent(Point.transform);
        quit.GetComponent<ButtonQuitScript>().PassInfo(zonecounterW, "ZoneW" + zonecounterW, 2); //1 forbidden

        Point.transform.SetParent( ZonesParentWalls[zonecounterW].transform);

        Point.name = "node" + counter;
        //Point.transform.LookAt(new Vector3(camera.transform.position.x, 0f, camera.transform.position.z));

        points.Add(Point.transform);

        verticesList.Add(finalPosition);
        //Instantiate the UI button to kill this thing
        quit.SetActive(false);

        counter++;
    }



    public void PlaceCeilingNode(Vector3 clickPoint)
    {
        var finalPosition = grid.GetNearestPointOnGrid(clickPoint); //position in grid
        Debug.Log("Click for ceiling");
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.AddComponent<ZONESCRIPT>();
        sphere.AddComponent<ButtonQuitScript>();
      

        sphere.name = "CeilingZone";
        sphere.tag = "DesignaLayer";
        sphere.transform.localScale = new Vector3(1, 1, 1);
        sphere.transform.position = finalPosition + height;
        sphere.GetComponent<MeshRenderer>().material = ceilingMaterial;
        sphere.transform.SetParent(ParenCeiling.transform);


        quit = Instantiate(PrefabButtonQuit, finalPosition + new Vector3(.5f, 0.5f, 0.5f), Quaternion.identity, ZonesFrobidden.transform);
        quit.transform.SetParent(sphere.transform);
        quit.GetComponent<ButtonQuitScript>().PassInfo(0, "ZoneC" + 0, 3);

    }

    public void createAMesh(int counter, GameObject ZonePater)

    {
  
        Mesh mesh = ZonePater.GetComponent<MeshFilter>().mesh;

        //this one should erase the very very last
        verticesList.RemoveAt(counter);
 
        mesh.vertices = verticesList.ToArray();
          
        Vector2[] uvs = new Vector2[verticesList.Count];

        //Calculate the triangles according to the num of vertices
 
        CalculatingTheTriangles(verticesList.Count);
        //assign it
        mesh.triangles = triangleArray;

        mesh.RecalculateNormals();
        //Depending on zone number, set the material
        if (NumZones == 1)
        {
            ZonePater.GetComponent<MeshRenderer>().material = ForbiddenZones.zoneMaterial;
        }
        else
        {
            ZonePater.GetComponent<MeshRenderer>().material = WallZones.zoneMaterial;
        }
        ZonePater.GetComponent<MeshCollider>().sharedMesh = mesh;


        //NOW MAKE ANOTHEER VERTICES LIST; BUT IN DIFFEREMT ORDER
        GameObject mgo = new GameObject();
        mgo.tag = "zone";
        mgo.transform.SetParent(ZonePater.transform);
       
         
    }

    public void CalculatingTheTriangles(int vertixSize)
    {

        //Debug.Log("Calculating with size " + vertixSize);

        if (vertixSize % 2 == 0)
        {
           //Debug.Log("Pair");
            int trianglesize = vertixSize - 2;
            int triangeSizeArray = trianglesize * 3;

          //  Debug.Log("how many triangle " + trianglesize);
           // Debug.Log("triangle size this big " + triangeSizeArray);

            triangleArray = new int[triangeSizeArray];

            int first = 0;
            int second = 1;

            for (int i = 0; i < triangleArray.Length; i+=3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = first;
                    first++;
                }
               
            }

            for (int i = 1; i < triangleArray.Length; i += 3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = second;
                    second++;
                }
            }

            for (int i = 2; i < triangleArray.Length; i += 3)
            {
                triangleArray[i] = vertixSize-1;
            }
 
        }
        else
        {
          //  Debug.Log("Unpair");
            int trianglesize = vertixSize - 1;
            int triangeSizeArray = trianglesize * 3;

            triangleArray = new int[triangeSizeArray];

          //  Debug.Log("how many triangle " + trianglesize);
          //  Debug.Log("triangle size this big " + triangeSizeArray);

            int first = 0;
            int second = 1;

            for (int i = 0; i < triangleArray.Length; i += 3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = first;
                    first++;
                }

            }

            for (int i = 1; i < triangleArray.Length; i += 3)
            {
                if (first < triangeSizeArray)
                {

                    triangleArray[i] = second;
                    second++;
                }
            }

            for (int i = 2; i < triangleArray.Length; i += 3)
            {
                triangleArray[i] = vertixSize - 1;
            }


        }
 

        triangleArray[0] = 0;
        /*
        for (int i = 0; i < triangleArray.Length; i ++)
        {
            Debug.Log("trinagle array index  [" + i + "]  VAlue:" + triangleArray[i]);
        }
        */
    }
 
    public void ForceTrianglesToBeMultipleOf3(int cunt, int vertexcunt)
    {
        if (cunt % 3 == 0)
        {
            triangleArray = new int[cunt];
            int j = 0;
            bool once =false;
            for (int i = 0; i < cunt; i++)
            {
                if (j < vertexcunt && !once  )
                {
                  //  Debug.Log("TriArr[" + i + "]" + "(" + j + ")");
                    triangleArray[i] = j;
                    j++;

                }
                else
                {
                    once = true;
                    j--;
                    triangleArray[i] = j;
                 //   Debug.Log("TriArr[" + i + "]" + "(" + j + ")");
                }


            }



        }
        else
        {
           //Debug.Log("this Cunt is recursive :"+cunt);
            cunt++;
            ForceTrianglesToBeMultipleOf3(cunt, vertexcunt);
        }


    }
 
    //THIS will be a UI button on top of the made zone
    //
    public void ButtonDelete(int index,int zoneID)
    {
        //Find the zone inside the Class and delete the index (logically)
        //find the nodes in parent and delete them gameobjects (physcally)
       
        if (zoneID == 1)
        {
            Debug.Log("Button Delete of " + index+" in zone forbidden");
            ForbiddenZones.Zones.RemoveAt(index);
             
            for (int i = 0; i < ZonesParentForbidden.Count; i++)
            {
                if (ZonesParentForbidden[i].name == "ZoneF"+index)
                { 
                    ZonesParentForbidden.RemoveAt(i);
                    break;
                }
            }

            zonecounterF++;

        }
        else if (zoneID == 2)
        {
            Debug.Log("Button Delete of " + index + " in zone walls");
            WallZones.Zones.RemoveAt(index);
        }
        else if (zoneID == 3)
        {
            Debug.Log("Button Delete of " + index + " in zone ceiling");
        }



    }
 

    /*
    public void CtrlZ()
    {
        if (counter <= 0)
        {

            Debug.Log("do nothing");
        }
        else
        {

            Debug.Log("counter " + counter + " lps counter " + lps.Count);
            //Points list, remove last one
            nodeToFind = parentOFNodes.transform.Find("node" + (counter - 1)).gameObject;
            Destroy(nodeToFind);
            points.RemoveAt(counter - 1);
            lps.RemoveAt(lps.Count - 1);
            counter--;

            Debug.Log("trying to re scale line of node " + (counter));
            //reset pivotline before the last one
            LinePivotScript LastLine = points[counter - 1].GetComponentInChildren<LinePivotScript>();
            LastLine.gameObject.transform.localScale = new Vector3(.1f, .1f, .1f);
        }
    }

    public void EraseAllNodes()
    {
        if (counter <= 0)
        {

            Debug.Log("do nothing");
        }
        else
        {

            points.Clear();
            foreach (Transform t in parentOFNodes.transform)
            {
                GameObject.Destroy(t.gameObject);
            }
            counter = 0;
        }
    }


    */
 
}

[Serializable]
public class NodeZone{

    public int id;
    public string type;
    public List<GameObject> Zones = new List<GameObject>();
    public Material zoneMaterial;

}
