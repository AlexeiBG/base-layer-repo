﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MatrixManager : MonoBehaviour
{
    #region UIDebugs
    //public InputField valor1;
    //public InputField valor2;
    //public Text result;



    int NumA = 0;
    int NumB = 0;

    //valores de lectura

    //Busqueda en matriz
    int searchA = 0;
    int searchB = 0;

    //Valores IMPORTANTES

    public float res;

    public float DryBulb = 0f;
    public float WetBulb = 0f;

    float Value_Right_DBinMatrix = 0f;
    float Value_Left_DBinMatrix = 0f;

    float Value_Up_WBinMatrix = 0f;
    float Value_Down_WBinMatrix = 0f;

    float SearchCenter = 0f;
    float UpValue = 0f;
    float LeftValue = 0f;
    float UpperLeftValue = 0f;

    float OrangeUP = 0f;
    float OrangeDown = 0f;
    float OrangeCenter = 0f;
    #endregion

    /*
    public int[,] Matriz = {
                            { 0, 1, 2, 3, 4, 5, 6, 7, 8 },
                            { 1,10,20,30,40,50,60,70,80 },
                            { 2,22,23,24,25,26,27,28,29 },
                            { 3,31,32,33,34,35,36,37,38 },
                            { 4,41,42,43,44,45,46,47,48 },
                            { 5,51,52,54,53,55,56,57,58 },
                            { 6,61,62,63,64,65,66,67,68 },
                            { 7,71,72,74,75,77,78,79,85 },
                            { 8,81,82,83,84,85,86,87,88 }

    };
    */
    public float[,] MatrizF = {
            { 0f,   28f,    30f,  32f,    34f,    36f,     38f,     40f,    42f   },
            { 18f, 26.1f,  23.7f,  21f,    18.6f,  16.2f,  13.5f,   11.1f,  8.4f  },
            { 20f, 33.9f,  31.2f,  28.8f,  26.1f,  23.7f,  21f,     18.6f,  15.9f },
            { 22f, 42.3f,  39.9f,  37.2f,  34.5f,  32.1f,  29.4f,   27f,    24.3f },
            { 24f, 51.6f,  49.8f,  46.2f,  43.8f,  41.1f,  38.4f,   36f,    33.3f },
            { 26f, 61.5f,   59.1f, 56.4f,  54f,    51.3f,  48.6f,   45.9f,  43.5f },
            { 28f, 72.6f,   61.9f, 67.2f,  64.8f,  61.1f,  59.4f,   56.7f,   54f  },
            { 30f,   0f,    81.9f, 79.2f,  76.5f,  73.8f,   71f,    68.4f,   66f  }
    };

    /// <summary>
    /// INT search
    /// </summary>
    /*
    public void LecturaA()
    {
        NumA = int.Parse(valor1.text);

    }

    public void lecturaB()
    {
        NumB = int.Parse(valor2.text);
    }

    public void Escritura()
    {

        result.text = "Busqueda en matriz [" + NumA + "][" + NumB + "] result " + Matriz[NumA, NumB];
        BusquedaEnEsquinas();


    }

    public void BusquedaEnEsquinas()
    {
        result.text += "!!! Esquina superior derecha : " + Matriz[(NumA - 1), (NumB + 1)] +
                      "  Esquina inferior derecha: " + Matriz[(NumA + 1), (NumB + 1)] +
                      "  Esquina superior iquierda : " + Matriz[(NumA - 1), (NumB - 1)] +
                      "  Esquina inferior izquierda : " + Matriz[(NumA + 1), (NumB - 1)];



    }
    */

    /// <summary>
    /// Search floats
    /// </summary>


    public void Get_DB_and_WB(float a, float b)
    {
        DryBulb = a;
        WetBulb = b;

        StartCoroutine(coroutine( a, b ) );
    }

    IEnumerator coroutine(float a, float b)
    {
        yield return null;
        BusquedaPorFloatsA(b);
        yield return null;
        BusquedaPorFloatsB(a);
        yield return null;
        BusquedaEnEsquinasMatrizF();
        yield return null;
    }


    public void BusquedaPorFloatsA(float FloatA)
    {
        //usa rangos antes de asignar un valor en la matriz
        Debug.Log("FLOAT A " + FloatA);
        if (FloatA <= 18)
        {
            searchA = 1;
           // Debug.Log("Search Y " + searchA);
        }
        else if (FloatA >= 18.1 && FloatA <= 20.1)
        {
            searchA = 2;
            //Debug.Log("Search Y " + searchA);
        }
        else if (FloatA >= 20.2 && FloatA <= 22.1)
        {
            searchA = 3;
            //Debug.Log("Search Y " + searchA);
        }
        else if (FloatA >= 22.2 && FloatA <= 24.1)
        {
            searchA = 4;
            //ebug.Log("Search Y " + searchA);
        }
        else if (FloatA >= 24.2 && FloatA <= 26.1)
        {
            searchA = 5;
            //Debug.Log("Search Y " + searchA);
        }
        else if (FloatA >= 26.2 && FloatA <= 28.1)
        {
            searchA = 6;
            //Debug.Log("Search Y " + searchA);
        }
        else if (FloatA >= 28.2 && FloatA <= 30.1)
        {
            searchA = 6;
            //Debug.Log("Search Y " + searchA);
        }
        else if (FloatA > 30.1)
        {
            searchA = 6;
        }

    }

    public void BusquedaPorFloatsB(float FloatB)
    {
        //usa rangos antes de asignar un valor en la matriz
        Debug.Log("FLOAT B " + FloatB);
        if (FloatB < 28)
        {
            searchB = 1;
           // Debug.Log("Search X " + searchB);
        }
        else if (FloatB >= 28.1 && FloatB <= 30.1)
        {
            searchB = 2;
           // Debug.Log("Search X " + searchB);
        }
        else if (FloatB >= 30.2 && FloatB <= 32.1)
        {
            searchB = 3;
           // Debug.Log("Search X " + searchB);
        }
        else if (FloatB >= 32.2 && FloatB <= 34.1)
        {
            searchB = 4;
           // Debug.Log("Search X " + searchB);
        }
        else if (FloatB >= 34.2 && FloatB <= 36.1)
        {
            searchB = 5;
           // Debug.Log("Search X " + searchB);
        }
        else if (FloatB >= 36.2 && FloatB <= 38.1)
        {
            searchB = 6;
           // Debug.Log("Search X " + searchB);
        }
        else if (FloatB >= 38.2 && FloatB <= 40.1)
        {
            searchB = 7;
            //Debug.Log("Search X " + searchB);
        }
        else if (FloatB >= 40.2 && FloatB <= 42.1)
        {
            searchB = 7;
        //    Debug.Log("Search X " + searchB);
        }
        else if (FloatB > 42.2)
        {
            searchB = 7;
        }


    }

    public void BusquedaEnEsquinasMatrizF()
    {
        /*
        Debug.Log( "!!! Arriba : " + MatrizF[(searchA - 1), (searchB)] + "\n"+
                     "  Izquierda: " + MatrizF[(searchA), (searchB - 1)] + "\n" +
                      "  Esquina superior iquierda : " + MatrizF[(searchA - 1), (searchB - 1)] + " \n");
        */
        ////////Values FOUND VALUES
        SearchCenter = MatrizF[searchA, searchB];
        UpValue = MatrizF[(searchA - 1), (searchB)];
        LeftValue = MatrizF[(searchA), (searchB - 1)];
        UpperLeftValue = MatrizF[(searchA - 1), (searchB - 1)];

        ///////values of DB
        Value_Right_DBinMatrix = MatrizF[0, searchB];
        Value_Left_DBinMatrix = MatrizF[0, (searchB - 1)];
        Value_Up_WBinMatrix = MatrizF[(searchA - 1), 0];
        Value_Down_WBinMatrix = MatrizF[searchA, 0];

        /*
        Debug.Log(" DB Right  " + Value_Right_DBinMatrix +"\n");
        Debug.Log(" DB Left  " + Value_Left_DBinMatrix + "\n");
        Debug.Log(" WB Down  " + Value_Down_WBinMatrix + "\n");
        Debug.Log(" WB Up  " + Value_Up_WBinMatrix + "\n");
        */
        //VAlores NARANAJAS
        OrangeUP = (((DryBulb - Value_Left_DBinMatrix) * (UpValue - UpperLeftValue)) / (Value_Right_DBinMatrix - Value_Left_DBinMatrix)) + UpperLeftValue;

        OrangeDown = (((DryBulb - Value_Left_DBinMatrix) * (SearchCenter - LeftValue)) / (Value_Right_DBinMatrix - Value_Left_DBinMatrix)) + LeftValue;

        OrangeCenter = (((WetBulb - Value_Up_WBinMatrix) * (OrangeDown - OrangeUP)) / (Value_Down_WBinMatrix - Value_Up_WBinMatrix)) + OrangeUP;

        res = OrangeCenter;
    }



}
