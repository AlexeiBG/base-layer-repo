﻿using UnityEngine;

public class wallLineSon : MonoBehaviour
{
    MeshRenderer mesh;
    public Material yellowWall;
    public Material blueWall;

    public void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        mesh.material = blueWall;
    }

    public void ChangeToYellow()
    {
        mesh.material = yellowWall;
    }

    public void ChangeToBlue()
    {
        mesh.material = blueWall;
    }
}
