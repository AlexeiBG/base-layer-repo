﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.IO;

public class CubePLacerExample : MonoBehaviour
{

    CustomGrid grid;
    [Header("General stuff")]
    public bool Paint = false;
    public Vector3 cubeScale;

    [Header("Forbidden Zone")]
    public Material forbiddenMaterial;
    public Transform forbiddenParent;
    public bool forbiddenZoneTurn = true;

    [Header("Free Zone")]
    public Material freeMaterial;
    public Transform freeParent;
    public bool freezoneTurn = false;

    [Header("Wall Zone")]
    public Transform wallParent;
    public Vector3 wallScale;
    public Material wallMaterial;
    public bool wallLayerTurn = false;

    [Header("Celing Zone")]
    public Material ceilingMaterial;
    public Vector3 ceilingNodePos;
    public bool CeilingTurn;
    public Transform ceilingParent;

    public void Awake()
    {
        grid = FindObjectOfType<CustomGrid>();
    }
 
    // Update is called once per frame
    void Update()
    {
        if (Paint)
        {
            if (Input.GetMouseButton(0))
            {
                RaycastHit hitInfo;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hitInfo))
                {
                    PlaceCubeNear(hitInfo.point);
                }

            }
            if (Input.GetMouseButton(1))
            {
                RaycastHit hitInfo;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hitInfo))
                {
                    Debug.Log("CUBEPLACERSCRIPT this is the tag " + hitInfo.collider.tag);
                    if (hitInfo.collider.tag == "DesignaLayer")
                    {
                        Destroy(hitInfo.collider.gameObject);
                    }

                }

            }

        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hitInfo;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hitInfo))
                {
                    PlaceCubeNear(hitInfo.point);
                }

            }
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit hitInfo;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hitInfo))
                {
                    Debug.Log("CUBEPLACERSCRIPT this is the tag " + hitInfo.collider.tag);
                    if (hitInfo.collider.tag == "DesignaLayer")
                    {
                        Destroy(hitInfo.collider.gameObject);
                    }

                }

            }

        }
    }


    private void PlaceCubeNear(Vector3 clickPoint)
    {
        var finalPosition = grid.GetNearestPointOnGrid(clickPoint);

        if (forbiddenZoneTurn == true)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            // MeshCollider cubecoll = cube.GetComponent<MeshCollider>();
            // Destroy(cubecoll);
            cube.tag = "DesignaLayer"; 
            cube.name = "ForbiddenZone";
            cube.transform.localScale = cubeScale;
            cube.transform.position = finalPosition;
            cube.GetComponent<MeshRenderer>().material = forbiddenMaterial;
            cube.transform.SetParent(forbiddenParent);

        }
        else if(freezoneTurn == true)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            // MeshCollider cubecoll = cube.GetComponent<MeshCollider>();
            // Destroy(cubecoll);
            cube.tag = "DesignaLayer";
            cube.name = "FreeZone";
            cube.transform.localScale = cubeScale;
            cube.transform.position = finalPosition;
            cube.GetComponent<MeshRenderer>().material = freeMaterial;
            cube.transform.SetParent(freeParent);

        }
        if (wallLayerTurn == true )
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            // MeshCollider cubecoll = cube.GetComponent<MeshCollider>();
            // Destroy(cubecoll);
            cube.tag = "DesignaLayer";
            cube.name = "wallZone";
            cube.transform.localScale = wallScale;
            cube.transform.position = finalPosition;
            cube.GetComponent<MeshRenderer>().material = wallMaterial;
            cube.transform.SetParent(wallParent);


        }

        if (CeilingTurn == true)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            sphere.name = "CeilingZone";
            sphere.tag = "DesignaLayer";
            sphere.transform.position = finalPosition+ceilingNodePos;
            sphere.GetComponent<MeshRenderer>().material = ceilingMaterial;
            sphere.transform.SetParent(ceilingParent);

        }

    }






}
