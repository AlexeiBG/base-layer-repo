﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slide : Transition {
	//The widht of the screen from the canvas
	private float screenWidth;

	void Start () {
		//Set the screen width from the RecTransform of the AdminCanvas  
		screenWidth = GetComponent<RectTransform>().rect.width;
		
		//Set the array of canvas and the transitionCanvas
		screensCanvas = GetComponent<AdminCanvas>().screens;
		transitionCanvas = GetComponent<AdminCanvas>().canvasTransition;

		//If it has a menu, assign the menu
		if(GetComponent<AdminCanvas>().hasMenuCanvas)
			menuCanvas = GetComponent<AdminCanvas>().menuCanvas;
			
	}

	//The canvas moves in the screen
	public void SlideTransitionIn(int index, float timeInSec){

		//We set the nextCanvas and the previousCanavas
		previousCanvas = screensCanvas[AdminCanvas.activeScreen];
		nextCanvas = screensCanvas[index];

		//Set the sortingOrder of the different Canvas
		previousCanvas.sortingOrder = 0;
		transitionCanvas.sortingOrder = 1;
		nextCanvas.sortingOrder = 2;

		if(menuCanvas!=null)
			menuCanvas.sortingOrder = 3;

		//Enable the transition and next canvas
		transitionCanvas.enabled = true;
		nextCanvas.enabled = true;

		//make the transition canvas visible and add a little transparency to the nextCanvas
		transitionCanvas.GetComponent<CanvasGroup>().alpha = 1;
		nextCanvas.GetComponent<CanvasGroup>().alpha = 0.8f;

		//Move the previous and next Canvas
		previousCanvas.transform.localPosition = new Vector3(0, 0, 0);
		nextCanvas.transform.localPosition = new Vector3(-Screen.width, 0, 0);
		
		//Start the courutine
		StartCoroutine ( SlideIn (index, timeInSec, screensCanvas));
	}

	//The canvas moves out of the screen
	public void SlideTransitionOut(int index, float timeInSec){

		//Set the nextCanvas and the previousCanavas
		previousCanvas = screensCanvas[AdminCanvas.activeScreen];
		nextCanvas = screensCanvas[index];

		//Set the sortingOrder of the different Canvas
		previousCanvas.sortingOrder = 2;
		transitionCanvas.sortingOrder = 1;
		nextCanvas.sortingOrder = 0;

		if(menuCanvas!=null)
			menuCanvas.sortingOrder = 3;

		//Enable the transition and next canvas
		transitionCanvas.enabled = true;
		nextCanvas.enabled = true;

		transitionCanvas.transform.localPosition = new Vector3 (0, 0, 0);

		//make the transition and next canvas visible
		transitionCanvas.GetComponent<CanvasGroup>().alpha = 1;
		nextCanvas.GetComponent<CanvasGroup>().alpha = 1;

		//Start the courutine
		StartCoroutine ( SlideOut (index, timeInSec));
	}

	//Coroutine moves out
	private IEnumerator SlideOut (int index, float timeInSec)
	{
		//Set the start and final position of the first movement
		Vector3 posInicial = new Vector3(0, 0, 0);
		Vector3 posFinal = new Vector3(screenWidth *0.1f, 0, 0);

		//Variable we use to make sure the tansition take a specific time
		float timeTransition = 0;

		//First Movement
		while(timeTransition<1){
			//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
			timeTransition += Time.deltaTime * (2/timeInSec);

			//Move the previous Canvas and add some transparency
			previousCanvas.transform.localPosition = Vector3.Lerp(posInicial, posFinal,timeTransition);
			previousCanvas.GetComponent<CanvasGroup>().alpha = 0.8f + ((1-timeTransition)*0.2f);
			yield return null;
		}

		//Move the next canvas to the center of the screen
		nextCanvas.transform.localPosition = new Vector3 (0, 0, 0);

		//Set the start and final position of the second movement
		posInicial = posFinal;
		posFinal = new Vector3(-screenWidth, 0, 0);

		//Reset the timeTransition
		timeTransition = 0;

		//Second movement
		while(timeTransition<1){
			//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
			timeTransition += Time.deltaTime * (2/timeInSec);

			//Move the previousCanvas out
			previousCanvas.transform.localPosition = Vector3.Lerp(posInicial, posFinal,timeTransition*timeTransition);

			//Make the transitionCanvas transparent
			transitionCanvas.GetComponent<CanvasGroup>().alpha = 1 - (timeTransition*timeTransition);

			yield return null;
		}

		//After the transitionCanvas has cover the whole screen, we enable the index canvas and disable all other canvas
		for (int i=0; i<screensCanvas.Length; i++)
		{
			if (i != index)
			{
				screensCanvas [i].enabled = false;
			}else{
				screensCanvas [i].enabled = true;
			}
		}

		//Make the previous Canvas visible again
		previousCanvas.GetComponent<CanvasGroup>().alpha = 1;

		//Disable the transitionCanvas
		transitionCanvas.enabled = false;
		//chage the inTransition variable to false
		AdminCanvas.inTransition = false;
		//Update the active screen
		AdminCanvas.activeScreen = index;

		yield return null;
	}

	//Coroutine move in
	private IEnumerator SlideIn (int index, float timeInSec, Canvas[] screensCanvas)
	{
		//Set the start and final position of the first movement
		Vector3 posInicial = new Vector3(-Screen.width, 0, 0);
		Vector3 posFinal = new Vector3(Screen.width *0.1f, 0, 0);
		
		//Variable we use to make sure the tansition take a specific time
		float timeTransition = 1;

		//First movement
		while(timeTransition>0){
			//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
			timeTransition -= Time.deltaTime * (2/timeInSec);

			//Move the nextCanvas
			nextCanvas.transform.localPosition = Vector3.Lerp(posInicial, posFinal,1-(timeTransition * timeTransition));

			//Make the transitionCanvas Visible
			transitionCanvas.GetComponent<CanvasGroup>().alpha = 1 - (timeTransition*timeTransition);
			yield return null;
		}

		//Set the start and final position of the second movement
		posInicial = posFinal;
		posFinal = new Vector3(0, 0, 0);

		timeTransition = 0;
		
		//Second movement
		while(timeTransition<1){
			//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
			timeTransition += Time.deltaTime * (2/timeInSec);

			//Move the next canvas and make it completely visible
			nextCanvas.transform.localPosition = Vector3.Lerp(posInicial, posFinal,timeTransition*timeTransition);
			nextCanvas.GetComponent<CanvasGroup>().alpha = 0.8f + (timeTransition*0.2f);
			yield return null;
		}

		//After the transitionCanvas has cover the whole screen, we enable the index canvas and disable all other canvas
		for (int i=0; i<screensCanvas.Length; i++)
		{
			if (i != index)
			{
				screensCanvas [i].enabled = false;
			}else{
				screensCanvas [i].enabled = true;
			}
		}

		//Make the previous Canvas visible again
		previousCanvas.GetComponent<CanvasGroup>().alpha = 1;

		//Disable the transitionCanvas
		transitionCanvas.enabled = false;
		//chage the inTransition variable to false
		AdminCanvas.inTransition = false;
		//Update the active screen
		AdminCanvas.activeScreen = index;

		yield return null;
	}
}
