﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminCanvas : MonoBehaviour {

   
    //This variable will determine how the canvas will change
    public enum ChangeCanvasForm
	{
		Scale,
		Roll,
		Slide,
		MoveUp
	};
	public ChangeCanvasForm changeCanvasType;

	//The starting Screen
    public  int mainScreen = 0;

   
	//The screen that is active in that movement
	public static int activeScreen;
	
	//The screen that were shown before and therefore are in the back
	public static List<int> backScreens;

	//The time it take for the transition to happend
	[ Range (0.1f,4) ]
	public float speedInSec = 1f;

	//Self explanatory
	public Canvas[] screens;
	public static bool inTransition;
	
	//The canvas that makes the trasition, if the transition requires it
	public  Canvas canvasTransition;
	//Canvas for a menu that stays in every canvas 
	public Canvas menuCanvas;
	//Determines if the application has a menu that stays in every canvas
	public bool hasMenuCanvas;

    //public losingAR lostAR;

    public void Start()
	{
        //lostAR = FindObjectOfType<losingAR>();

		// Disable all screens but the MainScreen
		for ( int i = 0; i < screens.Length; i++ )
		{
			if (i != mainScreen )
			{
				screens[i].enabled = false;
			}else{
				// Activate Main Screen
				screens[i].transform.localPosition = new Vector3 (0, 0, 0);
				screens[i].enabled = true;
			}
		}
		

		// Starts Back Screen List and search for PopUpManager
		backScreens = new List<int>();

		// Sets transition false
		inTransition = false;

		// Active Screen is the same as main screen
		activeScreen = mainScreen;

		// Vector2 screenRes;
		// screenRes.x = Screen.currentResolution.width;
		// screenRes.y = Screen.currentResolution.height;

		// GetComponent<CanvasScaler>().referenceResolution = screenRes;
		Debug.Log("Current resolution"+Screen.currentResolution);
		Debug.Log("Reference resolution"+GetComponent<CanvasScaler>().referenceResolution);
	}

	public void Update()
	{
		// If transition, exit
		if (inTransition)
			return;

		// When Escape key is pressed and no notificaions are shown go back to the preview screen...
		if (Input.GetKeyDown(KeyCode.Escape)&&(!inTransition))
		{
			GoBackCanvas();
		}
	}

	//The function that activates the transition to a a new canvas
	public void ChangeCanvas(int index){

		// If the anim is still moving screens then exit
		if ( inTransition )
			return;



		//If the screen index is the Active Screen then exit
		if( index == activeScreen )
			return;


		// If the screen is in back list then exit
		if ( backScreens.Count > 0 )
		{
			for(int i = 0; i < backScreens.Count; i++){
				if(backScreens[i]==index){
					GoBackCanvasIndex(index);
					return;
				}
			}
		}

		//Activate inTransition, add the current canvas to the backScreeens list, and set the activeScreen to the index
		inTransition = true;
		backScreens.Add( activeScreen );
		

		//call a function depending of the changeCanvasType
		switch (changeCanvasType)
		{
			case ChangeCanvasForm.Scale:
				GetComponent<Scale>().ScaleTransition(index, speedInSec);
				break;

			case ChangeCanvasForm.Roll:
				GetComponent<Roll>().RollTransition(index, speedInSec);
			break;
			case ChangeCanvasForm.Slide:
				GetComponent<Slide>().SlideTransitionOut(index, speedInSec);
			break;
			case ChangeCanvasForm.MoveUp:
				GetComponent<MovePartsUp>().MoveUpTransition(index, speedInSec);
				
			break;
		}
	}

	//If the canvas you want to transition was previewsly seen
	public void GoBackCanvasIndex(int index){

		// If the anim is still moving screens then exit
		if ( inTransition )
			return;
		
		//If the screen index is the Active Screen then exit
		if(index == activeScreen)
			return;

		if (backScreens.Count == 0)
			return;
		
		//Activate inTransition
		inTransition = true;

		// remove the index canvas from the backScreeens list
		for(int i = 0; i < backScreens.Count; i++){
			if(backScreens[i]== index){
				backScreens.RemoveAt(i);
			}
		}

		//call a function depending of the changeCanvasType
		switch (changeCanvasType)
		{
			case ChangeCanvasForm.Scale:
				GetComponent<Scale>().ScaleTransition(index, speedInSec);
				break;

			case ChangeCanvasForm.Roll:
				GetComponent<Roll>().RollTransition(index, speedInSec);
			break;
			case ChangeCanvasForm.Slide:
				GetComponent<Slide>().SlideTransitionIn(index, speedInSec);
			break;
			case ChangeCanvasForm.MoveUp:
				GetComponent<MovePartsUp>().MoveUpTransition(index, speedInSec);
			break;
		}
	}

	//Function that activated when you click the back button
	public void GoBackCanvas(){
		// If the anim is still moving screens then exit
		if ( inTransition )
			return;

		if (backScreens.Count == 0)
			return;

		int index  = backScreens[backScreens.Count - 1];
		backScreens.RemoveAt(backScreens.Count - 1);
		inTransition = true;

		//call a function depending of the changeCanvasType
		switch (changeCanvasType)
		{
			case ChangeCanvasForm.Scale:
				GetComponent<Scale>().ScaleTransition(index, speedInSec);
				break;
			case ChangeCanvasForm.Roll:
				GetComponent<Roll>().RollTransition(index, speedInSec);
			break;
			case ChangeCanvasForm.Slide:
				GetComponent<Slide>().SlideTransitionIn(index, speedInSec);
			break;
			case ChangeCanvasForm.MoveUp:
				GetComponent<MovePartsUp>().MoveUpTransition(index, speedInSec);
			break;
		}

	}

	public void SetChangeCanvasType(int transitionType){
		changeCanvasType = (ChangeCanvasForm) transitionType;
	}


}
