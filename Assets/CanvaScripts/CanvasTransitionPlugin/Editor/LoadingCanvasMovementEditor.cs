﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(LoadingTransition))]
public class LoadingCanvasMovementEditor : Editor {


	SerializedProperty exitCall;
	SerializedProperty inSecSpeed;
	SerializedProperty typeChangeCanvas;
	SerializedProperty outTime;
	SerializedProperty adminCanvas;
	SerializedProperty imageLoad;

	void OnEnable()
	{
		typeChangeCanvas = serializedObject.FindProperty("changeCanvasEffect");
		inSecSpeed = serializedObject.FindProperty ("speedInSec");
		exitCall = serializedObject.FindProperty("waitForExitCall");
		outTime = serializedObject.FindProperty("timeToExit");
		adminCanvas = serializedObject.FindProperty("canvasAdministrator");
		imageLoad = serializedObject.FindProperty("loadingImage");
	}


	public override void OnInspectorGUI()
	{
		// Actualiza los valores de la clase ConsultasAPI
		serializedObject.Update ();

		// Pone en el inspector las siguientes variables
		EditorGUILayout.PropertyField ( adminCanvas );
		EditorGUILayout.PropertyField ( imageLoad );
		EditorGUILayout.PropertyField ( inSecSpeed );
		EditorGUILayout.PropertyField ( typeChangeCanvas );
		EditorGUILayout.PropertyField ( exitCall );

		if(!exitCall.boolValue)
			EditorGUILayout.PropertyField ( outTime );




		serializedObject.ApplyModifiedProperties();
	}
}
