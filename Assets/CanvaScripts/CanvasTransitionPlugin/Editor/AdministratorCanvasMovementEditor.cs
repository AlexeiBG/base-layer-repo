﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(AdminCanvas))]
public class AdministratorCanvasMovementEditor : Editor 
{
	SerializedProperty screenMain;
	SerializedProperty screensEditor;
	SerializedProperty inSecSpeed;
	SerializedProperty typeChangeCanvas;
	SerializedProperty canvasTransition;
	SerializedProperty canvasMenu;
	SerializedProperty canvasMenuHas;

	void OnEnable()
	{
		screensEditor = serializedObject.FindProperty ("screens");
		screenMain = serializedObject.FindProperty ("mainScreen");	
		inSecSpeed = serializedObject.FindProperty ("speedInSec");
		typeChangeCanvas = serializedObject.FindProperty("changeCanvasType");
		canvasTransition = serializedObject.FindProperty("canvasTransition");
		canvasMenu = serializedObject.FindProperty("menuCanvas");
		canvasMenuHas = serializedObject.FindProperty("hasMenuCanvas");
	}


		public override void OnInspectorGUI()
	{
		// Actualiza los valores de la clase ConsultasAPI
		serializedObject.Update ();

		// Pone en el inspector las siguientes variables
		EditorGUILayout.PropertyField ( screensEditor, true );
		EditorGUILayout.PropertyField ( screenMain );
		EditorGUILayout.PropertyField ( inSecSpeed );
		EditorGUILayout.PropertyField ( canvasTransition );
		EditorGUILayout.PropertyField ( typeChangeCanvas );
		EditorGUILayout.PropertyField ( canvasMenuHas );

		if(canvasMenuHas.boolValue)
			EditorGUILayout.PropertyField ( canvasMenu );





	serializedObject.ApplyModifiedProperties();
	}

}
