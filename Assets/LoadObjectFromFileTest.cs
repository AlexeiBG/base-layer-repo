﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dummiesman;
using System.IO;
using System.Text;
using System;
using UnityEditor.UI;

public class LoadObjectFromFileTest : MonoBehaviour
{

    [Header("Models FOlder")]
    public string filename;
    public string FolderTextures;
    public string PathToObj;
    public string PathToMtl;

    [Header("Venue Folder")]
    public string venueFile;
    public string PathToVenueOBJ;
    public string PathToVenueMTL;
    public List<GameObject> VenueChildren;
  
    [Header("Objects to Replace")]
    public GameObject[] chairsGOs;
    string error = string.Empty;
    public GameObject loadedObj;
    public GameObject venueComplete;
    public Material scifishader;


    public void Start()
    {

       // Debug.Log("path "+ Application.dataPath );
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            //LoadLocal();
            LoadVenue();
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
             LoadLocal();
             
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            VenueCompleteChildren();
        }

    }


    public void LoadLocal()
    {
        //file path
        if (!File.Exists(PathToObj+filename+".obj" ))
        {
            Debug.Log("not found " + PathToObj + filename + ".obj");
            error = "File doesn't exist.";
            Debug.Log("eror"+error);
        }
        else
        {

            loadedObj = new OBJLoader().Load(PathToObj+filename+".obj" , PathToMtl+filename+".mtl");
            loadedObj.SetActive(false);
            Debug.Log("The loaded OBJ is called " + loadedObj.GetComponentInChildren<MeshFilter>().mesh.name);
            Debug.Log("The loaded OBJ is called " + loadedObj.GetComponentInChildren<MeshRenderer>().material);

        }
       
        foreach (GameObject go in chairsGOs)
        {
            go.GetComponent<MeshFilter>().mesh = loadedObj.GetComponentInChildren<MeshFilter>().mesh;
            loadedObj.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", Color.white);
            go.GetComponent<MeshRenderer>().material = loadedObj.GetComponentInChildren<MeshRenderer>().material;
        }

    }

    public void LoadVenue()
    {
        //file path
        if (!File.Exists(PathToVenueOBJ + venueFile + ".obj"))
        {

            error = "File doesn't exist.";
            Debug.Log("eror" + error + " Path "+PathToVenueOBJ+" "+venueFile+".obj");
        }
        else
        {
            venueComplete = new OBJLoader().Load(PathToVenueOBJ + venueFile + ".obj", PathToVenueMTL + venueFile + ".mtl");

            int childrenCount = venueComplete.transform.childCount;


            for (int i = 0; i < childrenCount; i++)
            {

                VenueChildren.Add(venueComplete.transform.GetChild(i).gameObject);
                if (venueComplete.transform.GetChild(i).GetComponent<Renderer>()) 
                {                   
                    venueComplete.transform.GetChild(i).GetComponent<Renderer>().material = scifishader;
                }
            }


         }
         
    }

    public void StringCHanger()
    {
        int counter = 0;
        StringBuilder sb = new StringBuilder();
        string lines = null;
        string paragraph = null;

        try
        {
            using (var sr = new StreamReader("Assets/Models/chairlow.mtl"))
            {
                //Debug.Log(sr.ReadToEnd());
                paragraph = sr.ReadToEnd();

                Debug.Log("paragrah complete " + paragraph);
                if (paragraph.Contains("map_Kd"))
                {
                    //
                }

                 
            }

        }
        catch(IOException e)
        {
            Debug.Log("catch exception e" + e);
        }

    }
   
    public void VenueCompleteChildren()
    {
        foreach (GameObject child in VenueChildren)
        {
            if (child.name == "venue_ceiling")
            {
                child.gameObject.SetActive(false);
            }
        }
    }


}
