﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meshapitry2 : MonoBehaviour
{

    Mesh awesomemesh;
    public bool clockwise = true;
    public Vector3[]  Vertices;
    public int[] triangles;
    int[] newTriangles;
    [Range(0, 2)]
    public int rotate = 0;

    private void Update()
    {
        if (clockwise)
        {
            triangles[0] = (0 + rotate) % 3;
            triangles[1] = (2 + rotate) % 3;
            triangles[2] = (1 + rotate) % 3;
        }
        else
        {
            triangles[0] = (0 + rotate) % 3;
            triangles[1] = (1 + rotate) % 3;
            triangles[2] = (2 + rotate) % 3;
        }
        UpdateMesh();
    }

    void Start()
    {
        awesomemesh = new Mesh();
        GetComponent<MeshFilter>().sharedMesh = awesomemesh;
        Vertices = new Vector3[3];
        Vertices[0] = new Vector3(0,0,0);
        Vertices[1] = new Vector3(1,0,0);
        Vertices[2] = new Vector3(0,0,1); 

 

        triangles = new int[3];
        triangles[0] = 0;
        triangles[1] = 2;
        triangles[2] = 1;

        UpdateMesh();
    }

    public void UpdateMesh()
    {
        awesomemesh.Clear();
        awesomemesh.vertices = Vertices;
        awesomemesh.triangles = triangles;

        awesomemesh.RecalculateNormals();
    }

}
