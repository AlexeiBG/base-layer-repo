﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonQuitScript : MonoBehaviour
{
    public int ZoneID;
    public int indexInLists;
    public string MyNameIs;
    public ZONESCRIPT myDadsName;
    public PointScript pointsScript;

    public void Awake()
    {
        pointsScript = FindObjectOfType<PointScript>();
    }

    public void Start()
    {
        myDadsName = GetComponentInParent<ZONESCRIPT>();

    }

    public void PassInfo(int index, string name, int id)
    {
        ZoneID = id;
        indexInLists = index;
        MyNameIs = name;
    }

    public void OnMouseDown()
    {

        Debug.Log("someonetouchme. this is my info: Indexinlist "
            +indexInLists+" mynameis "
            +MyNameIs+" mydadis"
            +myDadsName.name +" oh and i belong to "+ZoneID  );
        //pointsScript.ButtonDelete(indexInLists,ZoneID);
        Destroy(myDadsName.gameObject);
    }

}
